/*
Faça um programa que receba um número positivo e
maior que zero, calcule e mostre:
a) O número digitado ao quadrado;
b) O número digitado ao cubo;
c) A raiz quadrada do número digitado;
d) A raiz cúbica de número digitado.
*/

//Inicio
namespace exercicio_11
{
    const numero = 10;
    let numQ: number;

    numQ = numero *numero;
    numQ = Math.pow(numero, 2);
    numQ = numero ** 2;

    let numC: number;

    numC = numero * numero * numero;
    numC = Math.pow(numero, 3);
    numC = numero ** 3;

    let raizQ: number;

    raizQ = Math.sqrt(numero);

    let raizC: number;

    raizC = Math.cbrt(numero);

console.log(`o numero elevado ao quadrado: ${numQ} \n o numero elevado ao cubo: ${numC} \n a raiz quadrada do numero: ${raizQ} \n a raiz cubica do numero: ${raizC}`);
}
