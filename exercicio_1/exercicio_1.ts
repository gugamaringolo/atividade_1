//comentar
/*
Faça um programa que receba quatro números inteiros,
calcule e mostre a soma desses números.
*/

//primeiro bloco
//inicio
namespace exercicio_1
{
    //Entada dos dados
    //var---let---const
    let numero1, numero2, numero3, numero4: number;   

    numero1 = 4;
    numero2 = 8;
    numero3 = 16;
    numero4 = 32;

    let resultado: number;

    //processar os dados
    resultado = numero1 + numero2 + numero3 + numero4;

    //saida
    console.log("o resultado da soma é: " + resultado);
    //ou pode-se escrever
    console.log(`o resultado da soma é: ${resultado}`);
}